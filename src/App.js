import {Route, BrowserRouter, Routes, Navigate} from 'react-router-dom';
import Basket from "./Pages/BasketPage/Basket";
import Auth from "./Pages/AuthPage/Auth";
import ProductsPage from "./Pages/ProductsPage/ProductsPage";
import { useSelector} from "react-redux";



function App() {
   const isAuth = useSelector((state => state.products.isAuth));
  return (
    <BrowserRouter>
          {isAuth ?
              <Routes>
                <Route path="*" element={<Navigate to="/main" />}/>
                <Route path = '/' element={<Auth/>} />
                <Route path = '/basket' element={<Basket/>} />
                <Route path = '/main' element={<ProductsPage/>} />
              </Routes>
              :
              <Routes>
                <Route path="*" element={<Navigate to="/" />}/>
                <Route path = '/' element={<Auth/>} />
              </Routes>
          }
    </BrowserRouter>
  );
}

export default App;
