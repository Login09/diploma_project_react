import React from 'react';
import classes from './CardProductBasket.module.css';
import {useDispatch, useSelector} from "react-redux";
import { deleteBasket} from "../../redux/actions";


const CardProductBasket = (props) => {
    const dispatch = useDispatch();
    const {id, count, name, img, price} = props;
    const basket = useSelector((state) => state.products.basket);

    const deleteProduct = (product) => {
        const prevBasket = [...basket];
        let newBasket = [];
        for (let b of prevBasket) {
            if (b.id === product.id) {
                if (b.count === 1) {
                    continue;
                } else {
                    newBasket.push({...b, count: b['count'] - 1});
                }
            } else {
                newBasket.push(b);
            }
        };
        dispatch(deleteBasket(newBasket));
    }
    return (
        <div className={classes.Card}
          key={id}
        >
          <img src={process.env.REACT_APP_IMG_URL + img} width={122} height={122} alt={'#'}/>
          <h3 className={classes.Title}>{name}</h3>
            <p className={classes.Quantity}> * {count}</p>
            <p className={classes.Price}>{price} ₽</p>
          <div
              className={classes.Delete}
              onClick={() => deleteProduct(props)}
          ></div>
        </div>
    );
};

export default CardProductBasket;