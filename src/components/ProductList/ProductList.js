import React from 'react';
import classes from "./ProductList.module.css";
import CardProduct from "../CardProduct/CardProduct";
import {useSelector} from "react-redux";



const ProductList = () => {
    const products = useSelector((state => state.products.products));

    return (
        <div className={classes.Container}>
            {products.map(p => {
                const {id, img, name, description, price, weight} = p;
                return (
                 <CardProduct
                    key = {id}
                    id = {id}
                    img = {img}
                    name = {name}
                    description = {description}
                    price = {price}
                    weight = {weight}
                />
               )
            })}
        </div>
    );
};

export default ProductList;