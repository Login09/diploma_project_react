import React, {useState} from 'react';
import classes from './CardProduct.module.css';
import {useDispatch, useSelector} from "react-redux";
import {addBasket} from "../../redux/actions";


const CardProduct = (props) => {
    const [hover, setHover] = useState(false);
    const dispatch = useDispatch();
    const basket = useSelector(state => state.products.basket);
    const {name, img, weight, price, description} = props;

    const addBasketProduct = (product) => {
        const prevBasket = [...basket];
        let newBasket = [];
        let flag = false;
        for (let b of prevBasket) {
            if (b.id === product.id) {
                newBasket.push({...b, count: b['count'] ? b['count'] +1 : 1})
                flag = true;
            } else {
                newBasket.push(b)
            }
        };
        if (flag) {
            dispatch(addBasket(newBasket));
        } else {
            newBasket = [...newBasket, {...product, count:1}]
            dispatch(addBasket(newBasket));
        }
    }

    return (
        <div className={classes.Card}>
            <div className={classes.Card_wrapper}>
                <img src={process.env.REACT_APP_IMG_URL + img} width={268} height={270} alt={'#'}/>
             <div className={classes.Card_Description}>
                <h3
                    className={classes.Title}
                    style={hover ? {color:'#D58C51'} : null}
                >
                    {name}</h3>
                <p
                    className={classes.Description}
                    style={hover ? {color:'#D58C51'} : null}
                >
                    {description}</p>
            </div>
                <div className={classes.Block_price_btn}>
                  <div className={classes.Price_wt}>
                         <p
                             className={classes.Price}
                             style={hover ? {color:'#D58C51'} : null}
                         >
                             {price} ₽ / </p>
                         <p
                             className={classes.Weight}
                             style={hover ? {color:'#D58C51'} : null}
                         >
                             {weight}.</p>
                  </div>
                      <div className={classes.Wrapper_Circle}>
                          <div className={classes.Circle}
                               onMouseOver={(e) =>setHover(true)}
                               onMouseOut={(e) => setHover(false)}
                               onClick={() => addBasketProduct(props)}
                          ></div>
                      </div>
                </div>
            </div>
         </div>
    );
};

export default CardProduct;