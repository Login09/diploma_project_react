import React from 'react';
import classes from "./Button.module.css";

const Button = (setting) => {
    const cls = [
        classes.Button,
        classes[setting.type]
    ]
   const {text, onClick, disabled} = setting;
    return (
        <button
            onClick={onClick}
            className = {cls.join(' ')}
            disabled = {disabled}
        >
            {text}
        </button>
    );
};

export default Button;