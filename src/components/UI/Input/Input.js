import React from 'react';
import classes from "./Input.module.css";
const Input = (setting) => {
    const {type, placeholder, value, onChange, touched, valid, errorMessage} = setting;

    let cls = [classes.Input];
    if (!valid && touched) {
        cls = [
            classes.Input,
            classes.Error
        ];
    }
    return (
       <div>
        <input
            className={cls.join(' ')}
            type = {type}
            placeholder={placeholder}
            value = {value}
            onChange = {onChange}
        />
          {(!valid && touched)? <span style ={{color:'red'}}>{errorMessage}</span> : null}
       </div>
    );
};

export default Input;