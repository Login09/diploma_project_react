import {ADD_BASKET, AUTH_SETTING, DELETE_BASKET, SET_PRODUCTS} from "./types";

const intialState = {
    products: [],
    basket:[],
    isAuth:false
};

export const productsReducer = (state = intialState, action) => {
    switch (action.type) {
        case SET_PRODUCTS:
            return {...state, products: action.payload}
        case ADD_BASKET:
            return {...state, basket: action.payload}
        case DELETE_BASKET:
            return {...state, basket:action.payload}
        case  AUTH_SETTING:
            return {...state, isAuth: true}
        default: return state
    }
}