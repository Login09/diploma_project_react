import {ADD_BASKET, AUTH_SETTING, DELETE_BASKET, SET_PRODUCTS} from "./types";

export const setProducts = (products) => {
    return {
        type: SET_PRODUCTS,
        payload: products,
    };
};

export const addBasket = (basket) => {
    return {
        type: ADD_BASKET,
        payload: basket,
    };
}

export const deleteBasket = (basket) => {
    return {
        type: DELETE_BASKET,
        payload: basket,
    };
}

export const authorization = () => {
    return {
        type: AUTH_SETTING,
        payload: true,
    };
}
