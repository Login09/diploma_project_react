import React, {useEffect} from 'react';
import classes from './ProductsPage.module.css';
import logo from '../../assets/Vector.png'
import ProductList from "../../components/ProductList/ProductList";
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector} from "react-redux";
import {setProducts} from "../../redux/actions";
import {products} from "../../api";


const ProductsPage = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const basket = useSelector((state) => state.products.basket);
    let sum = basket.reduce((sum, b) => sum + (+b.price * b.count), 0)
    let count = basket.reduce((count, b) => count + (+b.count), 0)

    useEffect(() => {
       dispatch(setProducts(products));
    },[]);


    return (
       <>
        <div className={classes.ProductsPage}>
            <div className={classes.Container}>
                <header className={classes.Header}>
                  <h1 className={classes.Title}>НАША ПРОДУКЦИЯ</h1>
                   <div className={classes.Basket_Wrapper}>
                     <div className={classes.Basket_Text}>
                         <p className={classes.Basket_Count}>{count > 4 || count === 0 ? `${count} товаров` : `${count} товара`}</p>
                         <p className={classes.Basket_Summa}>на сумму {sum} ₽</p>
                     </div>
                     <div
                         className={classes.Circle}
                         onClick={() => navigate('/basket')}
                     >
                         <img src={logo} className={classes.Logo} alt={'#'} />
                     </div>
                   </div>
                </header>

                <ProductList/>

            </div>
        </div>
      </>
    );
};

export default ProductsPage;