import React, {useEffect, useState} from 'react';
import classes from "./Auth.module.css";
import authBackground from '../../assets/auth.jpg';
import Button from "../../components/UI/Button/Button";
import Input from "../../components/UI/Input/Input";
import {validateEmail, validatePass} from "../../utils/validator";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {authorization} from "../../redux/actions";

const Auth = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [email, setValidEmail] = useState({
        value:'',
        touched:false,
        valid:false
    });

    const [password, setValidPass] = useState({
        value:'',
        touched:false,
        valid:false
    });


    function onChangeEmail(e) {
        let valid = false;
        if (validateEmail(email.value)) {
            valid = true
        }
        setValidEmail(prev => {
            return {
                ...prev,
                value:e.target.value,
                touched:true,
                valid
            }
        })
    }

    const authorizations = () => {
        const user = JSON.parse(localStorage.getItem('user'));
        if (user.email === email.value && user.password === password.value) {
            dispatch(authorization());
            navigate('/main');
        } else {
            alert('Введите правильный логин или пароль')
        }

    }

    function onChangePass(e) {
        let valid = false;
        if (validatePass(e.target.value)) {
            valid = true
        }
        setValidPass(prev => {
            return {
                ...prev,
                value:e.target.value,
                touched:true,
                valid
            }
        })
    }

    useEffect(() => {
        const user = {email:'fk09@gmail.com', password :'12345678'}
        localStorage.setItem('user',  JSON.stringify(user));
    },[])


    return (
        <div className={classes.Auth} style={{ backgroundImage: "url(" + authBackground +")" }}>
                <div className={classes.Form}>
                    <h1 className={classes.Title}>Вход</h1>

                    <Input
                      type = {"email"}
                      placeholder = {"E-mail"}
                      onChange = {onChangeEmail}
                      touched = {email.touched}
                      valid = {email.valid}
                      value = {email.value}
                      errorMessage = {'Email невалидный'}
                    />
                    <Input
                      type = {"password"}
                      placeholder = {"Пароль"}
                      onChange = {onChangePass}
                      touched = {password.touched}
                      valid = {password.valid}
                      value = {password.value}
                      errorMessage = {'Пароль невалидный'}
                    />

                    <div className={classes.FormInput_check}>
                        <div className={classes.FormCheckbox_wrapper}>
                            <input
                                className={classes.FormCheckbox}
                                type="checkbox" id="input-checkbox"
                            />
                            <div className={classes.FormCheckbox_mark}></div>
                                <label className={classes.FormCheckbox_label} htmlFor="input-checkbox">
                                    Я согласен получать обновления на почту
                                </label>
                          </div>
                    </div>

                    <div className={classes.Button}>
                      <Button
                        text = {"Войти"}
                        type = {"success"}
                        disabled = {email.valid && password.valid ? false : true}
                        onClick = {authorizations}
                      />
                   </div>
                </div>
            </div>
      );
  };

export default Auth;