import React from 'react';
import classes from './Basket.module.css';
import CardProductBasket from "../../components/BasketCardProduct/CardProductBasket";
import Button from "../../components/UI/Button/Button";
import {useSelector} from "react-redux";

const Basket = () => {
    const basket = useSelector((state) => state.products.basket);
    let sum = basket.reduce((sum, b) => sum + (+b.price * b.count), 0)
    return (
        <div className={classes.Basket}>
            <div className={classes.Container}>
             <div className={classes.Wrapper}>
               <h1 className={classes.Title}>КОРЗИНА С ВЫБРАННЫМИ ТОВАРАМИ</h1>
                 {basket.map((b, i) => {
                     const {id, name, img,description, price, weight,count} = b;
                     return (
                         <CardProductBasket
                           key = {i}
                           id = {id}
                           name = {name}
                           img = {img}
                           description = {description}
                           price={price}
                           count={count}
                           weight = {weight}
                         />
                     )
                 })}
            </div>

            <footer className={classes.Footer}>
                <div className={classes.Footer_linia}></div>
                 <div className={classes.Order}>
                   <div className={classes.Footer_wrapper}>
                    <h3 className={classes.Order_title}>Заказ на сумму:</h3>
                    <h4 className={classes.Order_summa}> {sum} ₽</h4>
                    <Button
                        text = { "Оформить заказ"}
                        type = {'success'}
                     />
                 </div>
                </div>
              </footer>
            </div>
         </div>
    );
};

export default Basket;